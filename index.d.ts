/// <reference types="jquery"/>
/// <reference types="socket.io-client"/>
/// <reference types="tinymce"/>
/// <reference types="handlebars"/>
/// <reference types="howler"/>
/// <reference types="pixi.js"/>

import './types/constants';

// Apps

import './types/applications/application';
import './types/applications/baseentitysheet';
import './types/applications/formapplication';
import './types/applications/i18n';
import './types/applications/templates';

import './types/applications/forms/actor';
import './types/applications/forms/forms';
import './types/applications/forms/item';
import './types/applications/forms/permission';
import './types/applications/forms/roll-table-config';
import './types/applications/forms/scene';

import './types/applications/hud/chatbubble';
import './types/applications/hud/container';
import './types/applications/hud/controls';
import './types/applications/hud/hud';
import './types/applications/hud/menu';
import './types/applications/hud/navigation';
import './types/applications/hud/pause';
import './types/applications/hud/players';

import './types/applications/journal/journal-sheet';
import './types/applications/journal/note-config';

import './types/applications/placeables/placeables-config';
import './types/applications/placeables/placeables-hud';

import './types/applications/sidebar/apps/clientsettings';
import './types/applications/sidebar/apps/rolltabledirectory';
import './types/applications/sidebar/apps/actordirectory';
import './types/applications/sidebar/apps/combattracker';

import './types/applications/sidebar/sidebar';
import './types/applications/sidebar/sidebartab';
import './types/applications/sidebar/sidebardirectory';

// Core

import './types/core/audio';
import './types/core/config';
import './types/core/dicepool';
import './types/core/diceterm';
import './types/core/die';
import './types/core/fonts';
import './types/core/hooks';
import './types/core/keyboard';
import './types/core/roll';
import './types/core/settings';
import './types/core/socket';
import './types/core/sorting';
import './types/core/texteditor';
import './types/core/video';

// Framework

import './types/framework/collection';
import './types/framework/compendium';
import './types/framework/entity';
import './types/framework/entitycollection';

import './types/framework/entities/actor';
import './types/framework/entities/chatmessage';
import './types/framework/entities/combat';
import './types/framework/entities/folder';
import './types/framework/entities/item';
import './types/framework/entities/journal';
import './types/framework/entities/macro';
import './types/framework/entities/playlist';
import './types/framework/entities/rolltable';
import './types/framework/entities/scene';
import './types/framework/entities/user';
import './types/framework/entities/embeddedentity';

// PIXI

import './types/pixi/canvaslayer';
import './types/pixi/measuredtemplate';
import './types/pixi/mouseinteractionmanager';
import './types/pixi/placeableobject';
import './types/pixi/placeableslayer';

import './types/pixi/helpers/controlicon';
import './types/pixi/helpers/ray';
import './types/pixi/helpers/ruler';

import './types/pixi/placeables/token';
import './types/pixi/placeables/note';
import './types/pixi/placeables/tile';

// UI

import './types/ui/context';
import './types/ui/dialog';
import './types/ui/dragdrop';
import './types/ui/filepicker';
import './types/ui/notifications';
import './types/ui/tabs';
import './types/ui/activeeffectconfig';

//-- --//

import './types/game';
import './types/prototypes';
import './types/utils';
